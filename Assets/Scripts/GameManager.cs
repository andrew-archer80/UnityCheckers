﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public class GameManager : MonoBehaviour {

	// current camera as a static reference:
	public static Camera camCurr = null;

	// UI elements:
	UnityEngine.UI.Text textHelp = null;

	// checker selection, movement and destruction data:
	GameObject selectedChecker = null, selectedPosition = null,
	           destroyParticles = null;

	List<GameObject> selectedPath = new List<GameObject>();

	bool mouseLeftDown = false, selectedPositionDblClicked = false;
	float lastMouseLeftUpTime = 0.0f;

	bool movingAnim = false;

	[SerializeField]
	float moveAnimSpeed = 2.0f;

	// checkers on chessboard as game objects:
	List<GameObject> checkers = new List<GameObject>(12 + 12);

	GameObject FindChecker(string name) {
		foreach(GameObject checker in checkers)
			if(checker.name == name)
				return checker;
		return null;
	}

	// checkers game and state data:
	const uint CELL_IS_DISABLED     = 0xFFFF, // a light cell
		         CELL_IS_FREE         = 0x0000, // an unoccupied dark cell
		         WHITE_CHECKER        = 0x0010, // + checker index [1-12]
		         BLACK_CHECKER        = 0x0020, // + checker index [1-12]
		         SUPERIOR_CHECKER_BIT = 0x0040;

	uint[] chessboard = new uint[8*8];

	int whiteCheckerCount = 0, blackCheckerCount = 0;

	public enum GAME_STATE { NONE, WHITE_TURN, BLACK_TURN };

	public GAME_STATE gameState = GAME_STATE.NONE;

	public GAME_STATE GetGameState() { return gameState; }

	// checkers game functions:
	static int GetCellIndex(string name) {
		if(name.Length == 2 && name[0] >= 'A' && name[0] <= 'H' && name[1] >= '1' && name[1] <= '8')
			return (name[1] - '1') * 8 + (name[0] - 'A');
		Debug.Log("Invalid index name " + name);
		Debug.Assert(false);
		return -1;
	}


	/*static uint CheckerId(string name) { // id as stored in chessboard
		Debug.Assert(name.Length == 3 &&
			           (name[0] == 'w' || name[0] == 'W' || name[0] == 'b' || name[0] == 'B') &&
								 ('0' <= name[1] && name[1] <= '1' && '0' <= name[2] && name[2] <= '9'));
		uint id = (name[0] == 'w' || name[0] == 'W') ? WHITE_CHECKER : BLACK_CHECKER;
		if(name[0] == 'B' || name[0] == 'B')
			id |= SUPERIOR_CHECKER_BIT;
		uint n = (uint)((name[1]-'0')*10 + name[2]-'0');
		Debug.Assert(1 <= n && n <= 12);
		id += n;
		return id;
	}*/


	/*static string GetCheckerName(uint id) { // id as stored in chessboard
		string name = "";
		if((id & WHITE_CHECKER) == WHITE_CHECKER)
			name = "w";
		else if((id & BLACK_CHECKER) == BLACK_CHECKER)
			name = "b";
		else return "";
		if((id & SUPERIOR_CHECKER_BIT) == SUPERIOR_CHECKER_BIT)
			name = (name[0] == 'w' ? "W" : "B");
		id = (id & 0x00FF) + 1; // id = index
		if(id < 1 || id > 12)
			return "";
		name += (id<10 ? "0" : "") + id.ToString();
		return name;
	}*/


	bool ValidatePath(string path) { // TODO...
		Debug.Log("Validating path " + path);
		return path.Length > 0;
	}


	public void OnCheckerHitChecker(GameObject checker1, GameObject checker2) {
		GameObject victim =	selectedChecker == checker1 ? checker2 : checker1;
		int i = GetCellIndex(victim.GetComponent<CheckerPositionController>().position);
		if(chessboard[i] == CELL_IS_FREE)
			return; // already destroyed
		if((chessboard[i] & WHITE_CHECKER) == WHITE_CHECKER)
			--whiteCheckerCount;
		else
			--blackCheckerCount;
		chessboard[i] = CELL_IS_FREE;
		Debug.Log("killing  " + victim.name);
		GameObject particles = GameObject.Instantiate(destroyParticles, victim.transform.position, Quaternion.Euler(-90.0f, 0.0f, 0.0f));
		// Destroy the destruction particles after 2.5 seconds
		Destroy(particles, 2.5f);
		victim.SetActive(false); // not destroy but deactivate
	}


	// called once on start
	void Start () {
		camCurr = GameObject.Find("Cameras/Top").GetComponent<Camera>();
		Debug.Assert(camCurr != null);
		textHelp = GameObject.Find("Canvas/textHelp").GetComponent<UnityEngine.UI.Text>();
		Debug.Assert(textHelp != null);
		destroyParticles = (GameObject)AssetDatabase.LoadAssetAtPath("Assets/Prefabs/DestroyParticles.prefab", typeof(GameObject));
		Debug.Assert(destroyParticles != null);
		Init();
	}



	void ResetSelection() {
		movingAnim = false;
		mouseLeftDown = selectedPositionDblClicked = false;
		selectedPath.Clear();
		if(selectedChecker) {
			selectedChecker.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
			selectedChecker = null;
		}
		if(selectedPosition) {
			selectedPosition.GetComponent<Renderer>().material.DisableKeyword("_EMISSION");
			selectedPosition = null;
		}
	}



	void Update () {
		if(!movingAnim)
			return;

		Vector3 d = selectedPath[0].transform.localPosition - selectedChecker.transform.localPosition;
		if((d.x*d.x + d.z*d.z) < 0.001) { // almost at new destination...
			d = selectedPath[0].transform.localPosition;
			// update chessboard and checker position:
			int src_cell_index = GetCellIndex(selectedChecker.GetComponent<CheckerPositionController>().position),
				  dst_cell_index = GetCellIndex(selectedPath[0].name);
			uint cell_state = chessboard[src_cell_index];
			chessboard[src_cell_index] = CELL_IS_FREE;
			chessboard[dst_cell_index] = cell_state;
			selectedChecker.GetComponent<CheckerPositionController>().position = selectedPath[0].name;
			if(selectedChecker.name[0] == 'w' && selectedPath[0].name[1] == '8' ||
				 selectedChecker.name[0] == 'b' && selectedPath[0].name[1] == '1')
			{ // a checker is becoming superior
				chessboard[dst_cell_index] |= SUPERIOR_CHECKER_BIT;
				selectedChecker.transform.GetChild(0).gameObject.SetActive(true); // show crown
			}
			selectedPath.RemoveAt(0); // remove path head element
		}
		else
			d = selectedChecker.transform.localPosition + d * Time.deltaTime * moveAnimSpeed;
		d.y = selectedChecker.transform.localPosition.y; // correct elevation
		selectedChecker.transform.localPosition = d;
		if(selectedPath.Count == 0) { // after selectedChecker position has been updated, if path is empty - final destination reached
			ResetSelection();
			gameState = gameState == GAME_STATE.WHITE_TURN ? GAME_STATE.BLACK_TURN : GAME_STATE.WHITE_TURN; // switch turn
			textHelp.text = gameState == GAME_STATE.WHITE_TURN ? "White turn" : "Black turn";
		}
	}



	void FixedUpdate() {
		if(Input.GetKeyDown(KeyCode.R)) {
			Reset();
			return;
		}

		if(movingAnim || gameState == GAME_STATE.NONE)
			return;

		if(whiteCheckerCount == 0 || blackCheckerCount == 0) {
			gameState = GAME_STATE.NONE;
			textHelp.text = (whiteCheckerCount > 0 ? "White" : "Black") + " Won!\nPress 'R' to restart the game";
			return;
		}

		if(!Input.GetMouseButton(0)) { // left button released
			if(mouseLeftDown) {
				if(selectedPositionDblClicked && Time.time - lastMouseLeftUpTime < 0.5f) {
					// build path starting from selected checker position
					string path = selectedChecker.GetComponent<CheckerPositionController>().position;
					foreach(GameObject cell in selectedPath)
						path += cell.name;
					if(ValidatePath(path)) // if path is valid - start moving animation
						movingAnim = true;
					else // if path is not valid - just reset selection
						ResetSelection();
				}
				lastMouseLeftUpTime = Time.time;
				mouseLeftDown = false;
			}
			return;
		}	// else left button is pressed
		if(mouseLeftDown) // ignore if press state did not change since last call
			return;

    RaycastHit hitInfo = new RaycastHit();
    if(!Physics.Raycast(camCurr.ScreenPointToRay(Input.mousePosition), out hitInfo))
			return;
		GameObject selectedObj = hitInfo.transform.gameObject;
		if(selectedObj.name.Length == 3) { // a checker
			ResetSelection();
			char c = selectedObj.name[0];
			if(gameState == GAME_STATE.WHITE_TURN && !(c == 'w' || c == 'W') ||
				 gameState == GAME_STATE.BLACK_TURN && !(c == 'b' || c == 'B'))
				return; // wrong selection
			selectedChecker = selectedObj;
			selectedChecker.GetComponent<Renderer>().material.EnableKeyword("_EMISSION");
		}
		else { // must be a position
			Debug.Assert(selectedObj.name.Length == 2);
			if(!(selectedChecker && chessboard[GetCellIndex(selectedObj.name)] == CELL_IS_FREE))
				return; // if no checker is selected or dst position is not free - do nothing
			if(selectedPosition)
				selectedPosition.GetComponent<Renderer>().material.DisableKeyword("_EMISSION"); // deselect previously selected position
			selectedPositionDblClicked = selectedPosition == selectedObj; // trick to catch double click
			selectedPosition = selectedObj; // select a new position...
			selectedPosition.GetComponent<Renderer>().material.EnableKeyword("_EMISSION"); // ...and highlight it
			if(selectedPath.Count == 0 || selectedPath[selectedPath.Count-1].name != selectedPosition.name)
				selectedPath.Add(selectedPosition); // add selected position to path if it is not in the end of path already
			if(selectedPath.Count > 12) // reset selction if path becomes too long
				ResetSelection();
		}
		mouseLeftDown = true;
	}



	/*Initialize game objects at start*/
	void Init() {
		// assume chessboard centre at 0,0 and white and black checker samplers has proper y position
		GameObject cellDark = GameObject.Find("Chessboard/Cell-dark"),
			         cellLight = GameObject.Find("Chessboard/Cell-light"),
							 newObj = null;
		Vector3 pos = new Vector3(-3.5f, cellDark.transform.localPosition.y, -3.5f); // centre of the left-bottom cell

		// position the first 2 sampler cells
		cellDark.name = "A1";
		cellDark.transform.localPosition = pos;
		chessboard[GetCellIndex("A1")] = CELL_IS_FREE;
		pos.x += 1.0f;
		cellLight.name = "B1";
		chessboard[GetCellIndex("B1")] = CELL_IS_DISABLED;
		cellLight.transform.localPosition = pos;
		pos.x += 1.0f;
		
		// create and position other cells
		for(uint i = 1;  i <= 8;  ++i, pos.x = -3.5f, pos.z += 1.0f) {
			bool flip = i%2 == 0; // light-dark cell
			for(char c = (i > 1 ? 'A' : 'C');  c <= 'H';  ++c, pos.x += 1.0f, flip = !flip) {
				newObj = GameObject.Instantiate(flip ? cellLight : cellDark, pos, Quaternion.identity);
				newObj.name = c.ToString() + i.ToString();
				newObj.transform.parent = cellDark.transform.parent;
				chessboard[GetCellIndex(newObj.name)] = flip ? CELL_IS_DISABLED : CELL_IS_FREE;
			}
		}

		GameObject checkerWhite = GameObject.Find("Chessboard/Checker-white"),
						   checkerBlack = GameObject.Find("Chessboard/Checker-black");

		// rename the first white sampler checker
		checkerWhite.name = "w01";
		checkers.Add(checkerWhite);
		// create other white checkers
		for(uint i = 1, index = 2;  i <= 3;  ++i)
			for(char c = (i > 1 ? 'A' : 'C');  c <= 'H';  ++c, ++c) {
				newObj = GameObject.Instantiate(checkerWhite, checkerWhite.transform.localPosition, Quaternion.identity);
				newObj.name = "w" + (index<10 ? "0" : "") + index++.ToString(); // make names w01, ..., w12
				newObj.transform.parent = cellDark.transform.parent;
				newObj.SetActive(false);
				checkers.Add(newObj);
			}
		
		// rename the first black sampler checker
		checkerBlack.name = "b01";
		checkers.Add(checkerBlack);
		// create other black checkers
		for(uint i = 1, index = 2;  i <= 3;  ++i)
			for(char c = (i > 1 ? 'A' : 'C');  c <= 'H';  ++c, ++c) {
				newObj = GameObject.Instantiate(checkerBlack, checkerWhite.transform.localPosition, Quaternion.identity);
				newObj.name = "b" + (index<10 ? "0" : "") + index++.ToString(); // make names b01, ..., b12
				newObj.transform.parent = cellDark.transform.parent;
				newObj.SetActive(false);
				checkers.Add(newObj);
			}

		//Reset();
	}



	/*Reset the game to initial state*/
	void Reset() {
		ResetSelection();

		// clear chessboard
		for(int i = 0; i < chessboard.Length; ++i)
			if(chessboard[i] != CELL_IS_DISABLED)
				chessboard[i] = CELL_IS_FREE;
		whiteCheckerCount = blackCheckerCount = 0;

		// assume chessboard centre at 0,0 and white and black checkers has proper y position
		GameObject cellDark = GameObject.Find("Chessboard/A1"); // left-bottom cell
		Vector3 pos = cellDark.transform.position; // centre of the left-bottom cell
		pos.y = checkers[0].transform.localPosition.y; // corrected elevation

		// position white checkers
		for(uint i = 1;  i <= 3;  ++i, pos.x = cellDark.transform.position.x, pos.z += 1.0f)
			for(char c = 'A';  c <= 'H';  ++c, pos.x += 1.0f) {
				string pos_name = c.ToString() + i.ToString();
				if(chessboard[GetCellIndex(pos_name)] == CELL_IS_DISABLED)
					continue;
				// assign chessboard cell where checker is placed
				Debug.Assert(chessboard[GetCellIndex(pos_name)] == CELL_IS_FREE);
				chessboard[GetCellIndex(pos_name)] = WHITE_CHECKER + (uint)(++whiteCheckerCount);
				// reset checker game object
				selectedChecker = FindChecker("w" + (whiteCheckerCount<10 ? "0" : "") + whiteCheckerCount.ToString());
				selectedChecker.transform.localPosition = pos;
				selectedChecker.GetComponent<CheckerPositionController>().position = pos_name;
				selectedChecker.transform.GetChild(0).gameObject.SetActive(false); // hide crown
				selectedChecker.SetActive(true);
			}

		cellDark = GameObject.Find("Chessboard/H8");
		pos.x = cellDark.transform.position.x; // centre of the right-top cell
		pos.z = cellDark.transform.position.z;

		// position black checkers
		for(uint i = 8;  i >= 6;  --i, pos.x = cellDark.transform.position.x, pos.z -= 1.0f)
			for(char c = 'H';  c >= 'A';  --c, pos.x -= 1.0f) {
				string pos_name = c.ToString() + i.ToString();
				if(chessboard[GetCellIndex(pos_name)] == CELL_IS_DISABLED)
					continue;
				// assign chessboard cell where checker is placed
				Debug.Assert(chessboard[GetCellIndex(pos_name)] == CELL_IS_FREE);
				chessboard[GetCellIndex(pos_name)] = BLACK_CHECKER + (uint)(++blackCheckerCount);
				// reset checker game object
				selectedChecker = FindChecker("b" + (blackCheckerCount<10 ? "0" : "") + blackCheckerCount.ToString());
				selectedChecker.transform.localPosition = pos;
				selectedChecker.GetComponent<CheckerPositionController>().position = pos_name;
				selectedChecker.transform.GetChild(0).gameObject.SetActive(false); // hide crown
				selectedChecker.SetActive(true);
			}

		selectedChecker = null;
		gameState = GAME_STATE.WHITE_TURN;
		textHelp.text = "White turn";
	}


}
