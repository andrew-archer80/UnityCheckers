﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickHandler : MonoBehaviour {

	GameObject camTop, camSideWhite, camSideBlack;

	void Start() {
		// note: order of child cameras and buttons must be the following: 0 = Top, 1 = Side-white, 2 = Side-black
		GameObject gameObject = GameObject.Find("Cameras");
		camTop = gameObject.transform.GetChild(0).gameObject;
		camSideWhite = gameObject.transform.GetChild(1).gameObject;
		camSideBlack = gameObject.transform.GetChild(2).gameObject;
		gameObject = GameObject.Find("Buttons");
		gameObject.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(OnTopButtonClick);
		gameObject.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(OnSideWhiteButtonClick);
		gameObject.transform.GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(OnSideBlackButtonClick);
	}


	public void OnTopButtonClick() {
		camTop.GetComponent<ViewManager>().Reset();
		camTop.SetActive(true);
		GameManager.camCurr = camTop.GetComponent<Camera>();
		camSideWhite.SetActive(false);
		camSideBlack.SetActive(false);
	}

	public void OnSideWhiteButtonClick() {
		camSideWhite.GetComponent<ViewManager>().Reset();
		camSideWhite.SetActive(true);
		GameManager.camCurr = camSideWhite.GetComponent<Camera>();
		camSideBlack.SetActive(false);
		camTop.SetActive(false);
	}

	public void OnSideBlackButtonClick() {
		camSideBlack.GetComponent<ViewManager>().Reset();
		camSideBlack.SetActive(true);
		GameManager.camCurr = camSideBlack.GetComponent<Camera>();
		camSideWhite.SetActive(false);
		camTop.SetActive(false);
	}
}