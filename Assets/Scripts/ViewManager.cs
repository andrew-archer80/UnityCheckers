﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewManager : MonoBehaviour
{
	[SerializeField]
	Transform targetTransform;

	//[SerializeField]
	float rotationSpeed = 5.0f;
	//[SerializeField]
	float moveSpeed = 0.05f;
	//[SerializeField]
	float zoomSpeed = 1.5f;

	Vector3 rotation, rotation0;
	float distanceToOrigin, distanceToOrigin0;
	Vector3 chessboardPlaneDisplacement = Vector3.zero;


	void Start() {
		distanceToOrigin0 = transform.position.magnitude;
		rotation0 = transform.rotation.eulerAngles;
		Reset();
	}

	public void Reset() {
		distanceToOrigin = distanceToOrigin0;
		rotation = rotation0;
		chessboardPlaneDisplacement = Vector3.zero;
	}


	void Update() {
		//Vector3 position = transform.position;

		float dx = Input.GetAxis("Mouse X"),
			    dy = Input.GetAxis("Mouse Y");
		bool mouse_move = (dx != 0.0f || dy != 0.0f);

		if (Input.GetMouseButton(1) && mouse_move) { // right button
			dy *= rotationSpeed; // dy = drotation around right
			rotation.x -= dy;
			if(rotation.x > 90.0f)
				rotation.x = 90.0f;
			if(rotation.x < 10.0f)
				rotation.x = 10.0f;
			dx *= rotationSpeed; // dx = drotation around up
			rotation.y += dx;
			if(rotation.y >= 360.0f)
				rotation.y -= 360.0f;
			if(rotation.y <= -360.0f)
				rotation.y += 360.0f;
		}

		if(Input.GetMouseButton(2) && mouse_move) { // middle button
			dx *= moveSpeed;
			dy *= moveSpeed;
			// displacement in chessboard plane
			chessboardPlaneDisplacement.x -= dx;
			chessboardPlaneDisplacement.z -= dy;
			if(chessboardPlaneDisplacement.x > 12.0f)
				chessboardPlaneDisplacement.x = 12.0f;
			else if(chessboardPlaneDisplacement.x < -12.0f)
				chessboardPlaneDisplacement.x = -12.0f;
			if(chessboardPlaneDisplacement.z > 12.0f)
				chessboardPlaneDisplacement.z = 12.0f;
			else if(chessboardPlaneDisplacement.z < -12.0f)
				chessboardPlaneDisplacement.z = -12.0f;
		}

		dx = Input.GetAxis("Mouse ScrollWheel");
		if(dx != 0.0f) {
			dx *= zoomSpeed;
		// distance to origin
			distanceToOrigin += dx;
			if(distanceToOrigin > 12.0f)
				distanceToOrigin = 12.0f;
			else if(distanceToOrigin < 0.2f)
				distanceToOrigin = 0.2f;
		}


		// tilt, then move backward to the default distance
		transform.rotation = Quaternion.Euler(rotation);
		transform.position = - transform.forward * distanceToOrigin;

		// add displacement in chessboard plane
		Vector3 chessboardForward = transform.forward;
		chessboardForward.y = 0.0f;
		if(chessboardForward.magnitude > 1.0e-6f)
			chessboardForward = chessboardForward.normalized;
		else
			chessboardForward = transform.up;

		transform.position += transform.right * chessboardPlaneDisplacement.x; // chessboard 'right' from camera's view is the same as camera's 'right'
		transform.position += chessboardForward * chessboardPlaneDisplacement.z; // but chessboard 'forward' from camera's view is tricky
	}

}
