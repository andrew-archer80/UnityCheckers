﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckerPositionController : MonoBehaviour {

	[SerializeField]
	GameManager gameManager;

	public string position = ""; // current checker position as a chessboard cell name (e.g. A2, H7)

	enum CHECKER_TYPE { NONE, WHITE, BLACK };

	void OnTriggerEnter(Collider other) {
		CHECKER_TYPE t1 = CHECKER_TYPE.NONE, t2 = CHECKER_TYPE.NONE;
		if(this.CompareTag("Checker-black"))
			t1 = CHECKER_TYPE.BLACK;
		else if(this.CompareTag("Checker-white"))
			t1 = CHECKER_TYPE.WHITE;
		if(other.CompareTag("Checker-black"))
			t2 = CHECKER_TYPE.BLACK;
		else if(other.CompareTag("Checker-white"))
			t2 = CHECKER_TYPE.WHITE;
		if(gameManager.GetGameState() == GameManager.GAME_STATE.NONE ||
			 t1 == CHECKER_TYPE.NONE || t2 == CHECKER_TYPE.NONE || t1 == t2)
			return; // not in a game progress or not a white checker vs a black checker
		gameManager.OnCheckerHitChecker(this.gameObject, other.gameObject);
	}

}
